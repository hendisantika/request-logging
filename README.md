# Request Logging

### How to log incoming requests to all REST services in Spring Boot?

### Things todo list:

1. Clone this repository: `git clone https://gitlab.com/hendisantika/request-logging.git`
2. Navigate to the folder: `cd request-logging`
3. Run the application: `mvn clean spring-boot:run`
4. Open your POSTMAN APP.

### Image Screen shots

API Number One

![API Number One](img/api1.png "API Number One")

API Number Two

![API Number Two](img/api2.png "API Number Two")

Link Article:

* https://fullstackdeveloper.guru/2020/09/25/how-to-log-incoming-requests-to-all-rest-services-in-spring-boot/
* https://fullstackdeveloper.guru/2021/02/04/how-to-interpret-and-modify-rest-requests-in-spring-boot-in-a-centralized-way/