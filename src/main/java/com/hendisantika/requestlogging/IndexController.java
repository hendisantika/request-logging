package com.hendisantika.requestlogging;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * Project : request-logging
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 10/05/21
 * Time: 08.52
 */
@RestController
@RequestMapping
public class IndexController {
    @PostMapping(value = "/numberOneAPI")
    public Map<String, Object> numberOneAPI(@RequestBody Map<String, Object> request) throws Exception {
        return request;
    }

    @PostMapping(value = "/numberTwoAPI")
    public Map<String, Object> numberTwoAPI(@RequestBody Map<String, Object> request) throws Exception {
        return request;
    }

    @GetMapping
    public String index() {
        return "index";
    }
}
